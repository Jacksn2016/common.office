# common.office
java组件，实现Excel，word，pdf等常用office的

#目前只完善Excel的导入导出，方便简洁。具体步骤如下：
1，定义与Excel对应的bean类，加上配置。

@ExcelSheet(sheetIndex=0,titleIndex=1,dataIndex=2)

public class UploadUser {

    @ExcelColumn(fieldTitle = "登录名称", columnIndex = 0)

    private String username;

    @ExcelColumn(fieldTitle = "角色", columnIndex = 1)

    //@ColumnDataHandler("io.tsing.admin.util.RoleTranslate")

    private String role;

    @ExcelColumn(fieldTitle = "昵称", columnIndex = 2)

    private String nickname;

    @ExcelColumn(fieldTitle = "姓名", columnIndex = 3)

    private String realname;

    @ExcelColumn(fieldTitle = "性别", columnIndex = 4)

    private String gender;

    @ExcelColumn(fieldTitle = "密码", columnIndex = 5)

    //@ColumnDataHandler("io.tsing.admin.util.PassWordTranslate")

    private String password;

    @ExcelColumn(fieldTitle = "邮件", columnIndex = 6)

    private String email;

    @ExcelColumn(fieldTitle = "手机号码", columnIndex = 7)

    private String mobile;

    @ExcelColumn(fieldTitle = "电话号码", columnIndex = 8)

    private String telephone;


    get*(){}
    set*(*){}
}

2、调用导入导出方法，就可以实现导入导出了。

ExcelUtil excelUtil = new ExcelUtil();

导入：
InputStream inputStream=new FileInputStream(String.format("%s%s", path, "persionTempalte.xlsx"));

List<UploadUser>    s=excelUtil.parse(inputStream, UploadUser.class);


导出：
File file = new File(String.format("%s%s", path, "text.xlsx"));

OutputStream os = new FileOutputStream(file);

excelUtil.export(os, list, maps, null);


#就只要俩个步骤就可以实现导出导入，而且支持大数据的导出导入，还有可以个性化处理每一列的数据。



如果你喜欢本项目，请点击右上角的 Star，这样就可以将本项目放入您的收藏。

如果你非常喜欢，请点击右上角的 Fork，这样就可以直接将本项目直接复制到您的名下。

如果您有问题需要反馈，请点击 github 上的 issues 提交您的问题。

如果您改进了代码，并且愿意将它合并到本项目中，你可以使用 github 的 pull requests 功能来提交您的修改
